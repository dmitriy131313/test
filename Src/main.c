/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t temp[2];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void I2C_ClearBusyFlagErratum(I2C_HandleTypeDef* i2c);
void myTransmit(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	memset(temp, 0, 2);
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
//uint8_t temp[2];
HAL_StatusTypeDef err;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		myTransmit();
		
		//err = HAL_I2C_Master_Receive(&hi2c1, 0x81, temp, 1, 100);
//		if (err == HAL_BUSY)
//		{
//			//I2C_ClearBusyFlagErratum(&hi2c1);
//		}
		HAL_Delay(100);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void myTransmit(void)
{
	//__disable_irq();
	uint32_t cnt = 0;
	while (__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_BUSY) == SET)
	{
		if (cnt++ > 100000)
		{
			I2C_ClearBusyFlagErratum(&hi2c1);
			//HAL_Delay(2000);
			return;
		}
	}
		
	CLEAR_BIT(hi2c1.Instance->CR1, I2C_CR1_POS);
	
	SET_BIT(hi2c1.Instance->CR1, I2C_CR1_ACK);
		
	SET_BIT(hi2c1.Instance->CR1, I2C_CR1_START);
	cnt = 0;
	while (__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_SB) == RESET)
	{
		if (cnt++ > 100000)
		{
			I2C_ClearBusyFlagErratum(&hi2c1);
			//HAL_Delay(2000);
			return;
		}
	}
	(void) hi2c1.Instance->SR1;
	hi2c1.Instance->DR = I2C_7BIT_ADD_READ(0x81);   /////////////ADRESS!!!!!!!!!!!!!!!!!!!!1
		
	cnt = 0;
	while (__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_ADDR) == RESET)
	{
		if (cnt++ > 100000)
		{
			I2C_ClearBusyFlagErratum(&hi2c1);
			//HAL_Delay(2000);
			return;
		}
	}
		
	//(void) hi2c1.Instance->SR1;
	//(void) hi2c1.Instance->SR2;

	//CLEAR_BIT(hi2c1.Instance->CR1, I2C_CR1_ACK);
		//__disable_irq();

      /* Clear ADDR flag */
	(void) hi2c1.Instance->SR1;
	(void) hi2c1.Instance->SR2;
  //__HAL_I2C_CLEAR_ADDRFLAG(&hi2c1);

      /* Generate Stop */
  SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
		
//__enable_irq();
///////////////////////////////////Reading//////////////////////////////////////////
	cnt = 0;
	while (__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_RXNE) == RESET)
	{
		if (cnt++ > 100000)
		{
			I2C_ClearBusyFlagErratum(&hi2c1);
			//HAL_Delay(2000);
			return;
		}
	};
	temp[0] = (uint8_t)hi2c1.Instance->DR;
//=======================================================
		
	CLEAR_BIT(hi2c1.Instance->CR1, I2C_CR1_ACK);	
	//SET_BIT(hi2c1.Instance->CR1, I2C_CR1_ACK);
	cnt = 0;
	while (__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_RXNE) == RESET)
	{
		if (cnt++ > 100000)
		{
			I2C_ClearBusyFlagErratum(&hi2c1);
			//HAL_Delay(2000);
			return;
		}
	};
	temp[1] = (uint8_t)hi2c1.Instance->DR;
	
	SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
		
//	__HAL_I2C_CLEAR_ADDRFLAG(&hi2c1);
//		
//	if	(__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_BTF) == SET)
//	{temp[1] = (uint8_t)hi2c1.Instance->DR;}
//		SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);

      /* Re-enable IRQs */
     // __enable_irq();
}


void I2C_ClearBusyFlagErratum(I2C_HandleTypeDef* i2c)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	 //__enable_irq();
  // 1. Clear PE bit.
	CLEAR_BIT(i2c->Instance->CR1, I2C_CR1_PE);

  //  2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
  GPIO_InitStructure.Mode         = GPIO_MODE_OUTPUT_OD;
	//GPIO_InitStructure.Mode 				= GPIO_MODE_AF_OD;
  //GPIO_InitStructure.Pin 					= GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStructure.Pull         = GPIO_PULLUP;
  GPIO_InitStructure.Speed        = GPIO_SPEED_FREQ_HIGH;

  GPIO_InitStructure.Pin 					= GPIO_PIN_8;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);

  GPIO_InitStructure.Pin          = GPIO_PIN_9;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);

  // 3. Check SCL and SDA High level in GPIOx_IDR.
  while (GPIO_PIN_SET != HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8))
  {
    __asm("nop");
		//HAL_Delay(1);
  }

  while (GPIO_PIN_SET != HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9))
  {
    __asm("nop");
		//HAL_Delay(1);
  }

  // 4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);

  //  5. Check SDA Low level in GPIOx_IDR.
  while (GPIO_PIN_RESET != HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9))
  {
    __asm("nop");
		//HAL_Delay(1);
  }

  // 6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);

  //  7. Check SCL Low level in GPIOx_IDR.
  while (GPIO_PIN_RESET != HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8))
  {
    __asm("nop");
		//HAL_Delay(1);
  }

  // 8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);

  // 9. Check SCL High level in GPIOx_IDR.
  while (GPIO_PIN_SET != HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8))
  {
    __asm("nop");
		//HAL_Delay(1);
  }

  // 10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR).
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);

  // 11. Check SDA High level in GPIOx_IDR.
  while (GPIO_PIN_SET != HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9))
  {
    __asm("nop");
		//HAL_Delay(1);
  }

  // 12. Configure the SCL and SDA I/Os as Alternate function Open-Drain.
  GPIO_InitStructure.Mode         = GPIO_MODE_AF_OD;
  //GPIO_InitStructure.Alternate    = I2C_PIN_MAP;

  GPIO_InitStructure.Pin          = GPIO_PIN_8;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.Pin          = GPIO_PIN_9;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

  // 13. Set SWRST bit in I2Cx_CR1 register.
  //i2c->instance.Instance->CR1 |= 0x8000;
	SET_BIT(i2c->Instance->CR1, I2C_CR1_SWRST);

  __asm("nop");
	//HAL_Delay(1);

  // 14. Clear SWRST bit in I2Cx_CR1 register.
	CLEAR_BIT(i2c->Instance->CR1, I2C_CR1_SWRST);
  //i2c->instance.Instance->CR1 &= ~0x8000;

  __asm("nop");
	//HAL_Delay(1);

  // 15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register
  //i2c->instance.Instance->CR1 |= 0x0001;
	SET_BIT(i2c->Instance->CR1, I2C_CR1_PE);

  // Call initialization function.
  HAL_I2C_Init(i2c);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
